import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { userModel } from './../model/userModel';
import { personService } from './../service/personService';
import { AuthService } from './../auth/auth.service';
 
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})

export class LoginComponent {

  user3: userModel;
  user2={
    email: null,
    password: null,
    token: null,
    passwordNew: null,
  };
  passwordNew;
  email;
  url;
  key;
  password;

  constructor(private router : Router, private user: userModel, private personService: personService, private auth: AuthService){

  }

  login(form: NgForm){
    var nameUser = form.value.email;
    //console.log('value form: ',form.value);
    this.personService.login(form.value)
                      .subscribe(
                        user => {
                          this.user2 = user;
                          this.auth.setToken(this.user2.token);
                          sessionStorage.setItem(this.key = 'token', this.auth.getToken());
                          //console.log('passwordNew: ', this.passwordNew);
                          //console.log('user show: ', user, ' ,user2:', this.user2);
                          localStorage.setItem(this.email = 'email', this.user2.email);
                          localStorage.setItem(this.password = 'password', this.user2.password);
                          localStorage.setItem(this.password = 'passwordHash', this.user2.passwordNew);
                          this.router.navigate(this.url = ['/list']);
                        },
                        error =>  console.log(<any>error)
                      );  
  }
}
