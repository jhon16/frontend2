import { Component, ViewContainerRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastsManager, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';

import { AuthService } from './auth/auth.service';
import { personService } from './service/personService'
import { log } from 'util';
import { userModel } from './model/userModel';
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  constructor(translate:TranslateService, private router: Router,private toastr:ToastsManager, vRef: ViewContainerRef, private auth: AuthService, private personService: personService){
    this.toastr.setRootViewContainerRef(vRef);
    translate.setDefaultLang('en');
    translate.use('en');
  }

  key;
  url;
  email;
  password;
  confirmpassword;
  user2 = userModel;
  user = {
    email: localStorage.getItem('email'),
    password: null,
    passwordNew: null
  };

  Updatepassword(user, confirmpassword){
    if(this.user.password === localStorage.getItem('password')){
      if(this.user.passwordNew === confirmpassword){
        this.personService.updatePassword(user)
                          .subscribe(
                            () => {
                              this.toastr.success("New password doesn't match", 'SUCCESS!');
                              this.toastr.info("The session will close", 'INFO!',{toastLife: 10000});
                              this.logout();
                            },
                            error =>  console.log(<any>error)
                          );
      }else{
        this.toastr.error("New password doesn't match", 'ERROR!');
      }
    }else{
      this.toastr.error('Current password incorrect', 'ERROR!');
    }
  }

  logout(){
    localStorage.removeItem(this.email = 'email');
    sessionStorage.removeItem(this.key = 'token');
    localStorage.removeItem(this.key = 'token');
    localStorage.removeItem(this.password = 'password');
    localStorage.removeItem(this.password = 'passwordHash');
    this.router.navigate(this.url = ['/']);
  }

}
