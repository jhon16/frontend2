export class userModel {
  
    _id: string;
    email: string;
    password: string;
    token: string;
    passwordNew: string;
  
    constructor() {
        this._id = "";
        this.email = "";
        this.password = "";
        this.token = "";
        this.passwordNew = "";
    }
  }