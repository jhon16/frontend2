export class personModel {

  _id: string;
  name: string;
  lastName: string;
  email: string;
  gender: string;
  dateBorn: Date;
  image: string;
  dateStart: Date;
  salary: number;
  uid: string;
  dateBornCalculation: string;
  dateStartCalculation: string;

  constructor() {
      this._id = "";
      this.name = "";
      this.lastName = "";
      this.email = "";
      this.gender = "";
      this.dateBorn = null;
      this.image = "";
      this.dateStart = null;
      this.salary = 0;
      this.uid = "";
      this.dateBornCalculation = "";
      this.dateStartCalculation = "";
  }
}