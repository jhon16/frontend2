import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { personModel } from './../model/personModel';
import { userModel } from './../model/userModel';
import { AuthService } from './../auth/auth.service';
import { log } from 'util';

@Injectable()
export class personService{
  
  constructor(private http:Http, private userModel: userModel, private auth: AuthService){
  }

  private headers = new Headers({
    'Authorization':'Bearer '+ sessionStorage.getItem('token')
  });
  private Url = 'http://localhost:3000/people';
  private loginUrl = 'http://localhost:3000/login';

  login(form: userModel): Observable<userModel>{
    //console.log('form: ', form);
    return this.http.post(this.loginUrl, form)
                    .map(response => response.json() as userModel)
                    .catch(this.handleError);
  }

  Show(): Observable<personModel[]>{
    //console.log('token', sessionStorage.getItem('token'))
    return this.http.get(this.Url,{headers: this.headers})
                    .map(response => response.json() as personModel[])
                    .catch(this.handleError);
  }

  Register(Person: personModel): Observable<personModel>
  {
    return this.http.post(this.Url,Person,{ headers: this.headers })
                    .map(response => response.json() as personModel)
                    .catch(this.handleError);
  }

  deletePerson(person: personModel): Observable<any> {
    let updateUrl = `${this.Url}/${person._id}`;
    return this.http.delete(updateUrl, {headers: this.headers})
                    .map(this.success)
                    .catch(this.handleError);
}

  Update(Person: personModel): Observable<any>{
    let updateUrl = `${this.Url}/${Person._id}`;
    return this.http.put(updateUrl,Person, { headers: this.headers })
                    .catch(this.handleError);
  }

  updatePassword(User: userModel): Observable<any>{
    let updateUrl = `${this.Url}/null/password`;
    return this.http.put(updateUrl, {headers: this.headers})
                    .catch(this.handleError);
    }

  Calculate(Person: personModel, Type : String): Observable<any>{
    let updateUrl = `${this.Url}/dateCalculation/${Person._id}?field=${Type}`;
    return this.http.get(updateUrl,{headers : this.headers})
                    .map(res => {
                      return res;
                    })
                    .catch(this.handleError);
  }

  private success(): Observable<any> {
    console.log('succes');
    return Observable.create();
  }

  private handleError(response: Response): Observable<any> {
    console.log('response: ' , response);
    let errorMessage = `${response.status} - ${response.statusText}`;
    console.log('Error Handler: ',errorMessage);
    return Observable.throw(errorMessage);
  }
}