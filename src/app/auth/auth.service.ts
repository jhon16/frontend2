import { Injectable } from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';
import { userModel } from '../model/userModel';
//import decode from 'jwt-decode';

@Injectable()
export class AuthService {
  
  constructor(){

  }

  key;
  token;

  public setToken(auth): void{
    this.token = auth;
  }

  public getToken(): string {
    return this.token;
  }
  public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    // return a boolean reflecting 
    // whether or not the token is expired
    return tokenNotExpired(null, token);
  }
}
