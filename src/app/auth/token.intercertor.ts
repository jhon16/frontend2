import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';
import { personService } from '../service/personService';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private auth: AuthService, private personService: personService, private router: Router) {

  }

  url;

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    if(this.auth.getToken() === null){
      //this.auth.setToken("");
      //this.router.navigate(this.url = ['/']);
      //console.log('ingreso en verdadero, el token es: ', this.auth.getToken());
    }else{
      //console.log('ingreso en falso, el token es: ', this.auth.getToken());
      request = request.clone(
        {
          setHeaders: 
          {
            authorization: `Bearer ${this.auth.getToken()}`
          }
        });
        //console.log('request: ', request);
        return next.handle(request);
    }  
  }
}