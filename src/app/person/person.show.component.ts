import { Component, OnInit , ViewContainerRef } from '@angular/core';
import { ToastsManager, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { TranslateService } from '@ngx-translate/core' ;

import { personService } from './../service/personService';
import { personModel } from './../model/personModel';

@Component({
  selector: 'app-person-show',
  templateUrl: './person.show.component.html'
})

export class PersonShowComponent implements OnInit{

  ModelPerson = {
    _id: null,
    name:null,
    lastName: null,
    gender: null,
    dateBorn: null,
    image: null,
    dateStart: null,
    salary: null,  
  };
  
  isNewForm: boolean;
  people: personModel[];
  formPerson: personModel;
  time = {
    id: "",
    time: ""
  };

  _id;

  constructor(private personService: personService, private translate:TranslateService, private toastr:ToastsManager, vRef: ViewContainerRef){
    var people = this.people;
    this.toastr.setRootViewContainerRef(vRef);
    translate.setDefaultLang('en');
    translate.use('en');
  }

  ngOnInit() {
    if(sessionStorage.getItem('token') === null){
    }else{
      this.personService.Show()
                      .subscribe(
                        people => {
                          this.people = people
                        },
                        error =>  console.log(<any>error)
                      );                      
    }
  }

  Edit(Person: personModel){
    if(!Person) {
      Person = new personModel();
    }
    this.formPerson = Person;
    this.ModelPerson = Person;
  }

  Delete(Person: personModel){
    console.log('Entro en delete, id:', Person._id);
    if(!Person) {
      Person = new personModel();
    }
    this._id = Person._id;
    this.formPerson = Person;
    console.log('_id: ', this.formPerson._id);
    this.ModelPerson = Person;
    console.log('_id: ', this.ModelPerson._id);  
  }

  cancel(){
    this.isNewForm = false;
  }

  removePerson(person: personModel) {
    this.personService.deletePerson(person)
                      .subscribe(
                        () => {this.removePersonFromList(person),
                        this.toastr.info('Person data were delete', 'INFORMATION!');},
                        error => {
                          console.log(error);
                          this.toastr.info('Person data were not delete', 'INFORMATION!');
                        }
                      );
    
  }

  private removePersonFromList(person: personModel) {
    var index = this.people.indexOf(person, 0);
    if (index > -1) {
        this.people.splice(index, 1);
    }
  }

  Update(person: personModel){
    this.personService.Update(person)
                      .subscribe(
                        () => {
                          this.toastr.info('Person data were updated!', 'Success!');
                        },
                        error => {
                          console.log(error); 
                          this.toastr.error("error", 'Error!');
                        }

                      );
    this.isNewForm = false;
    this.formPerson = null;

  }

  validate(person: personModel, Type : "",Type2: ""){
    person.dateBornCalculation = "";
    if(person._id == Type){
      person.dateBornCalculation = Type2;
      return true;
    }else{
      return true;
    }
  }

  Calculate(person: personModel, Type: String){
    this.personService.Calculate(person,Type)   
                      .subscribe(
                        (res) => {
                          this.time.id = String(person._id),
                          this.time.time = res._body
                          console.log('time.id: ', this.time.id , ' y time.time: ', this.time.time);
                        },
                        (error) => console.log(error)
                      ); 
  }

}