import { Component, OnInit ,ViewContainerRef} from '@angular/core';
import { ToastsManager, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core' ;
import { Http } from '@angular/http';

import { personModel } from './../model/personModel';
import { personService } from './../service/personService';
//import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-person-create',
  templateUrl: './person.create.component.html'
})


export class PersonCreateComponent implements OnInit {

  
  ModelPerson = {
    name:null,
    lastName: null,
    gender: null,
    email: null,
    dateBorn: null,
    Image: null,
    dateStart: null,
    salary: null,  
  };
  people: personModel[];
  formPerson: personModel;
  url;
  
  constructor(private personService: personService, public router: Router, translate:TranslateService, public toastr:ToastsManager, vRef: ViewContainerRef,/* private auth: AuthService*/){
    this.toastr.setRootViewContainerRef(vRef);
    translate.setDefaultLang('en');
    translate.use('en');
  }
  
  ngOnInit() {
  }

  Register(person: personModel){
    if(person.name === "" || person.lastName === "" || person.email === "" || person.gender === "" || person.dateBorn === null || person.dateStart === null || person.salary === 0)
    {
      this.toastr.info('you must fill all the fields','INFO!');
    }else{
      this.personService.Register(person)
                        .subscribe((Registered) => {
                          this.router.navigateByUrl('/list');
                          this.people.push(Registered);                             
                        },
                          error => {console.log(error)
                        });
    }
  }
}