import { BrowserModule } from '@angular/platform-browser';
import { NgModule , ErrorHandler } from '@angular/core';
import {   } from '@angular/core/jasmine';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule , Routes } from '@angular/router';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { ModalModule } from 'ng2-modal';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { tokenNotExpired } from 'angular2-jwt';

import { AppComponent } from './app.component';
import { personService} from './service/personService';
import { PersonCreateComponent } from './person/person.create.component';
import { PersonShowComponent } from './person/person.show.component';
import { LoginComponent } from './login/login.component';
import { personModel } from './model/personModel';
import { userModel } from './model/userModel';
import { LoginGuard } from './login.guard';
import { NoLoginGuard } from './no-login.guard';
import { AuthService } from './auth/auth.service';
import { TokenInterceptor } from './auth/token.intercertor';


import {routes} from './routes/app.routes';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
  
@NgModule({
  declarations: [
    AppComponent,
    PersonCreateComponent,
    PersonShowComponent,
    LoginComponent,
    //AuthService,
    //TokenInterceptor
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    TranslateModule.forRoot({
      loader:{
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ToastModule.forRoot(),
    ModalModule,
  ],

  providers: [
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    personService,
    PersonCreateComponent,
    PersonShowComponent,
    LoginComponent,
    personModel,
    HttpClientModule,
    LoginGuard,
    NoLoginGuard,
    userModel,        
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
