import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class NoLoginGuard implements CanActivate {


  constructor(public router: Router){
    
  }

  key;
  url;

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean 
    {
      if(localStorage.getItem(this.key='email')===null){
        return true;
      }else{
        this.router.navigate(this.url = ['/list']);
        return false;
      }
  }
}
