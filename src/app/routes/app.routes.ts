import { NgModule } from '@angular/core';
import { RouterModule , Routes } from '@angular/router';

import { PersonCreateComponent } from './../person/person.create.component';
import { PersonShowComponent } from './../person/person.show.component';
import { LoginComponent } from './../login/login.component';
import { LoginGuard } from './../login.guard';
import { NoLoginGuard } from './../no-login.guard';

export const routes: Routes=
[
  { path: '' , component: LoginComponent, canActivate: [NoLoginGuard]},
  { path: 'list' , component: PersonShowComponent, canActivate: [LoginGuard]},
  { path: 'create' , component: PersonCreateComponent, canActivate: [LoginGuard]},
];